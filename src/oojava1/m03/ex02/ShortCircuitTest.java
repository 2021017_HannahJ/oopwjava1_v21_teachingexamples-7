package oojava1.m03.ex02;
/**
 * Class to demonstrate Short-circuit'ing of logical expressions
 * @author jarih
 *
 */
public class ShortCircuitTest {

	public static void main(String[] args) {
		int n = 0, x = 0, y = 0;
		System.out.printf("Initial values: x = %d   y = %d\n", x,y);
		while ( (++x<5) || (++y<5) ) {
			n++;
			System.out.printf("Iteration %d   : x = %d   y = %d\n", n,x,y);
		}
	}
}
